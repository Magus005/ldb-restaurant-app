Rails.application.routes.draw do
  resources :restaurants
  get '/restaurants/findby/:name' => 'restaurants#findby'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
