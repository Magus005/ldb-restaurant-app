class Restaurant
  include Mongoid::Document
  include Mongoid::Timestamps
  field :name, type: String
  field :borough, type: String
  field :cuisines, type: String
  field :address, type: Object
  field :grades, type: Object
end
