class RestaurantsController < ApplicationController
  before_action :set_restaurant, only: [:show, :update, :destroy]
  before_action :set_restaurant_name, only: [:findby]

  # GET /restaurants
  def index
    @restaurants = Restaurant.all

    render json: @restaurants
  end

  # GET /restaurants/1
  def show
    render json: @restaurant
  end

  # POST /restaurants
  def create
    @restaurant = Restaurant.new(restaurant_params)
    puts "create restaurant"
    puts @restaurant
    if @restaurant.save
      render json: @restaurant, status: :created, location: @restaurant
    else
      render json: @restaurant.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /restaurants/1
  def update
    if @restaurant.update(restaurant_params)
      render json: @restaurant
    else
      render json: @restaurant.errors, status: :unprocessable_entity
    end
  end

  
  # GET /restaurants/findby/:name
  def findby
    puts "findby restaurant_params : "
    render json: @restaurant
  end

  # DELETE /restaurants/1
  def destroy
    @restaurant.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_restaurant
      @restaurant = Restaurant.find(params[:id])
    end

    def set_restaurant_name
      @restaurant = Restaurant.find_by!(name: params[:name])
    end

    # Only allow a list of trusted parameters through.
    def restaurant_params
      params.require(:restaurant).permit(:name, :borough, :cuisines, :address => [:street, :building], :grades => [:grade, :score])
    end
end
