(0..100).each do |p|
    Restaurant.create(name: Faker::Restaurant.name, cuisines: Faker::Restaurant.type, borough: Faker::Address.community, address: { street: Faker::Address.street_address, building: Faker::Address.building_number }, grades: { grade: 'grade', score: 5 } )
   # puts "Created #{p}"
end