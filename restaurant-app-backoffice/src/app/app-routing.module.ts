import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddRestaurantComponent } from './restaurant/add-restaurant/add-restaurant.component';
import { DetailRestaurantComponent } from './restaurant/detail-restaurant/detail-restaurant.component';
import { EditRestaurantComponent } from './restaurant/edit-restaurant/edit-restaurant.component';
import { ListRestaurantComponent } from './restaurant/list-restaurant/list-restaurant.component';

const routes: Routes = [
  { path: '', redirectTo: 'restaurants', pathMatch: 'full' },
  { path: 'restaurants', component: ListRestaurantComponent },
  { path: 'restaurants/add', component: AddRestaurantComponent },
  { path: 'restaurants/edit/:id', component: EditRestaurantComponent },
  { path: 'restaurants/detail/:id', component: DetailRestaurantComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
