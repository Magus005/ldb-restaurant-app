import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-edit-restaurant',
  templateUrl: './edit-restaurant.component.html',
  styleUrls: ['./edit-restaurant.component.scss']
})
export class EditRestaurantComponent implements OnInit {
  restaurant: any = {};
  restaurantForm: FormGroup;
  data: any = {};
  restaurantId: any;

  constructor(private router: Router, private route: ActivatedRoute,
              private http: HttpClient, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.restaurantId = this.route.snapshot.paramMap.get('id');
    this.getOneRestaurant(this.restaurantId);
  }

  getOneRestaurant(id) {
    console.log('_id', id);
    this.http.get(`${environment.apiUri}/restaurants/${id}`).subscribe(
      (response) => {
        console.log(response['address']['street']);
        this.restaurant = response;
        this.initForm(this.restaurant);
      },
      (error) => console.log(error)
    );
  }

  gotoListRestaurantPage() {
    this.router.navigate(['/restaurants']);
  }

  initForm(restaurant) {
    this.restaurantForm = this.formBuilder.group({
      name: [`${restaurant.name}`, Validators.required ],
      cuisines: [`${restaurant.cuisines}`, Validators.required ],
      borough: [`${restaurant.borough}`, Validators.required ],
      building: [`${restaurant['address']['building']}`, Validators.required ],
      street: [`${restaurant['address']['street']}`, Validators.required ],
      grade: [`${restaurant['grades']['grade']}`, Validators.required ],
      score: [`${restaurant['grades']['score']}`, Validators.required ],
    });

    this.data = {
      name: restaurant.name,
      cuisines: restaurant.cuisines,
      borough: restaurant.borough,
      building: restaurant['address']['building'],
      street: restaurant['address']['street'],
      grade: restaurant['grades']['grade'],
      score: restaurant['grades']['score']
    };
  }


  updateRestaurant() {
    const restaurant = {
      name: this.data.name,
      cuisines: this.data.cuisines,
      borough: this.data.borough,
      address: {
        building: this.data.building,
        street: this.data.street
      },
      grades: {
        grade: this.data.grade,
        score: this.data.score
      }
    };

    console.log('restaurant', restaurant);
    this.http.put(`${environment.apiUri}/restaurants/${this.restaurantId}`, restaurant).subscribe(
      (response) => {
        console.log(response);
        this.router.navigate(['/restaurants']);
      },
      (error) => console.log(error)
    );
  }

}
