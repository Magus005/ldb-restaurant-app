import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';


@Component({
  selector: 'app-list-restaurant',
  templateUrl: './list-restaurant.component.html',
  styleUrls: ['./list-restaurant.component.scss']
})
export class ListRestaurantComponent implements OnInit {
  restaurants: any = [];
  searchText: any;

  constructor(private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    this.findAllRestaurants();
  }

  deleteRestaurant(id) {
    console.log('_id', id);
    this.http.delete(`${environment.apiUri}/restaurants/${id}`).subscribe(
      (response) => {
        console.log(response);
        window.location.href = '/restaurants';
      },
      (error) => console.log(error)
    );
  }

  gotoEditRestaurantPage(id) {
    console.log('_id', id);
    this.router.navigate([`/restaurants/edit/${id}`]);
  }
  findAllRestaurants() {
    this.http.get(`${environment.apiUri}/restaurants`).subscribe(
      (response) => {
        console.log(response);
        this.restaurants = response;
      },
      (error) => console.log(error)
    );
  }

  gotoAddRestaurantPage() {
    console.log('gotoAddRestaurantPage');
    this.router.navigate(['/restaurants/add']);
  }

}
