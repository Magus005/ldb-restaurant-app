import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-restaurant',
  templateUrl: './add-restaurant.component.html',
  styleUrls: ['./add-restaurant.component.scss']
})
export class AddRestaurantComponent implements OnInit {
  restaurantForm: FormGroup;
  data: any = {};

  constructor(private router: Router, private formBuilder: FormBuilder, private http: HttpClient) { }

  ngOnInit(): void {
    this.restaurantForm = this.formBuilder.group({
      name: ['', Validators.required ],
      cuisines: ['', Validators.required ],
      borough: ['', Validators.required ],
      building: ['', Validators.required ],
      street: ['', Validators.required ],
      grade: ['', Validators.required ],
      score: ['', Validators.required ]
    });
  }

  gotoListRestaurantPage() {
    this.router.navigate(['/restaurants']);
  }



  addRestaurant() {
    const restaurant = {
      name: this.data.name,
      cuisines: this.data.cuisines,
      borough: this.data.borough,
      address: {
        building: this.data.building,
        street: this.data.street
      },
      grades: {
        grade: this.data.grade,
        score: this.data.score
      }
    };

    console.log('restaurant', restaurant);
    this.http.post(`${environment.apiUri}/restaurants`, restaurant).subscribe(
      (response) => {
        console.log(response);
        this.router.navigate(['/restaurants']);
      },
      (error) => console.log(error)
    );
  }

}
