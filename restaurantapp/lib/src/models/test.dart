class RestaurantItem {
  final int id;
  final String name;
  final String cuisines;
  final String borough;
  final Object address;
  final Object grades;
  final DateTime updated_at;
  final DateTime created_at;

  RestaurantItem({
    required this.id,
    required this.name,
    required this.cuisines,
    required this.borough,
    required this.address,
    required this.grades,
    required this.updated_at,
    required this.created_at,
  });
}
