class RestaurantModel {
  List<RestaurantItem> _results = [];

  RestaurantModel.fromJson(List<dynamic> parsedJson) {
    List<RestaurantItem> temp = [];
    for (int i = 0; i < parsedJson.length; i++) {
      RestaurantItem result = RestaurantItem(parsedJson[i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<RestaurantItem> get results => _results;
  void setResults(List<RestaurantItem> value) {
    _results = value;
  }
}

class RestaurantItem {
  Map<String, dynamic> _id = {};
  String _name = "";
  String _cuisines = "";
  String _borough = "";
  Map<String, dynamic> _address = {"street": "street", "building": "building"};
  Map<String, dynamic> _grades = {"grade": "grade", "score": 5};
  String _updated_at = "";
  String _created_at = "";

  RestaurantItem(result) {
    _id = result['_id'];
    _name = result['name'];
    _cuisines = result['cuisines'];
    _borough = result['borough'];
    _address = result['address'];
    _grades = result['grades'];
    _updated_at = result['updated_at'];
    _created_at = result['created_at'];
  }

  String get name => _name;
  String get cuisines => _cuisines;
  String get borough => _borough;
  Map<String, dynamic> get address => _address;
  Map<String, dynamic> get grades => _grades;
  String get updated_at => _updated_at;
  String get created_at => _created_at;

  Map<String, dynamic> get id => _id;
}

class Address {
  String _street = "";
  String _building = "";

  Address(result) {
    _street = result['street'];
    _building = result['building'];
  }

  String get street => _street;
  String get building => _building;
}

class Grades {
  String _grade = "";
  int _score = 0;

  Grades(result) {
    _grade = result['grade'];
    _score = result['score'];
  }

  String get grade => _grade;
  int get score => _score;
}
