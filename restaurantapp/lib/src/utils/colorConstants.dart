import 'package:flutter/material.dart';

class ColorConstants {
  static const primaryColor = const Color(0xffF79352);
  static const backgroundColor = const Color(0xffF79955);
  static const shadowColor = const Color(0xffF7CBAB);
}
