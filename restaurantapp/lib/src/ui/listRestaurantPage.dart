import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:restaurantapp/src/blocs/restaurantBloc.dart';
import 'package:restaurantapp/src/models/restaurant_model.dart';
import 'package:restaurantapp/src/ui/widgets/restaurantTitle.dart';
import 'package:restaurantapp/src/utils/colorConstants.dart';

class ListRestaurantPage extends StatefulWidget {
  @override
  _ListRestaurantPageState createState() => _ListRestaurantPageState();
}

class _ListRestaurantPageState extends State<ListRestaurantPage> {
  var isSearch = false;
  List<RestaurantItem> listRestaurant = [];
  List<RestaurantItem> allRestaurant = [];
  List<RestaurantItem> filterRestaurant = [];
  int i = 1;
  @override
  void initState() {
    super.initState();
    listRestaurant = allRestaurant;
    bloc.fetchAllRestaurants();
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: ColorConstants.backgroundColor,
        body: StreamBuilder(
          stream: bloc.allRestaurants,
          builder: (context, AsyncSnapshot<RestaurantModel> snapshot) {
            if (snapshot.hasData) {
              allRestaurant = snapshot.data!.results.toList();
              listRestaurant = snapshot.data!.results.toList();
              return _buildBody();
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return Center(child: CircularProgressIndicator());
          },
        ));
  }

  Widget _buildBody() {
    return SafeArea(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
          Container(
            margin: EdgeInsets.symmetric(vertical: 30, horizontal: 15),
            padding: EdgeInsets.symmetric(horizontal: 30, vertical: 5),
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(29.5)),
            child: TextField(
              decoration: InputDecoration(
                  hintText: "Search",
                  hintStyle: TextStyle(
                    color: Colors.black.withAlpha(120),
                    fontSize: 18,
                  ),
                  icon: SvgPicture.asset(
                    'assets/images/search.svg',
                    height: 25,
                  ),
                  border: InputBorder.none),
              onChanged: (String keyword) {
                if (keyword.isEmpty) {
                  listRestaurant.clear();
                  filterRestaurant = allRestaurant;
                } else {
                  listRestaurant.clear();
                  filterRestaurant = allRestaurant
                      .where((user) =>
                          user.name.toLowerCase() == (keyword.toLowerCase()))
                      .toList();
                  listRestaurant.addAll(filterRestaurant);
                }
                // Refresh the UI
                setState(() {
                  listRestaurant = filterRestaurant;
                });
              },
            ),
          ),
          Expanded(
              child: Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
            child: Column(
              children: <Widget>[_buildIntro(), Expanded(child: _buildList())],
            ),
          )),
        ]));
  }

  Widget _buildIntro() {
    return Container(
      padding: EdgeInsets.all(20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Liste Restaurant Dakar",
            style: GoogleFonts.varelaRound(
                fontSize: 30, fontWeight: FontWeight.bold),
          )
        ],
      ),
    );
  }

  Widget _buildList() {
    if (i == 1) {
      listRestaurant.clear();
      listRestaurant = allRestaurant;
      i = 0;
    } else {
      listRestaurant = filterRestaurant;
    }
    if (listRestaurant.length > 0) {
      return Container(
        child: ListView.builder(
          padding: EdgeInsets.all(20),
          itemCount: listRestaurant.length,
          itemBuilder: (BuildContext context, int index) {
            return GridTile(
                child: InkResponse(
              enableFeedback: true,
              child: RestaurantTitle(
                restaurantItem: listRestaurant[index],
              ),
            ));
          },
        ),
      );
    } else {
      return Container(
        width: 10000,
        padding: EdgeInsets.all(20),
        margin: EdgeInsets.symmetric(vertical: 150),
        child: Text(
          "Aucun résultat trouvé",
          style: GoogleFonts.varelaRound(
              fontSize: 18, fontWeight: FontWeight.bold),
          textAlign: TextAlign.center,
        ),
      );
    }
  }
}
