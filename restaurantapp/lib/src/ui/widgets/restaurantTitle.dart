import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:restaurantapp/src/models/restaurant_model.dart';
import 'package:restaurantapp/src/utils/colorConstants.dart';

class RestaurantTitle extends StatelessWidget {
  final RestaurantItem restaurantItem;

  const RestaurantTitle({Key? key, required this.restaurantItem})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Card(
            elevation: 3,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            shadowColor: ColorConstants.shadowColor,
            child: Container(
              width: double.infinity,
              padding: EdgeInsets.symmetric(horizontal: 25, vertical: 30),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    restaurantItem.name,
                    style: GoogleFonts.varelaRound(
                        fontWeight: FontWeight.bold, fontSize: 22),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Text(
                    "Adresse: ${restaurantItem.borough}, ${restaurantItem.address['street']} ",
                    style: GoogleFonts.varelaRound(
                        fontWeight: FontWeight.bold,
                        fontSize: 12,
                        color: ColorConstants.primaryColor),
                  )
                ],
              ),
            )),
        Positioned(
          right: 10,
          top: 10,
          child: Image.asset(
            'assets/images/restaurant.png',
            height: 50,
            width: 50,
          ),
        )
      ],
    );
  }
}
