import 'dart:async';

import 'package:restaurantapp/src/models/restaurant_model.dart';
import 'package:restaurantapp/src/resources/restaurantApiProvider.dart';

class Repository {
  final restaurantsApiProvider = RestaurantApiProvider();

  Future<RestaurantModel> fetchAllRestaurants() =>
      restaurantsApiProvider.fetchRestaurantList();
}
