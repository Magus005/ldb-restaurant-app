import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' show Client, Response;
import 'package:restaurantapp/src/models/restaurant_model.dart';

class RestaurantApiProvider {
  Client client = Client();
  // final _baseUrl = "http://127.0.0.1:3000";

  Future<RestaurantModel> fetchRestaurantList() async {
    Response response;
    var url = "https://ldb-restaurant-app-api.herokuapp.com/restaurants";
    response = await client.get(Uri.parse(url));
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return RestaurantModel.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }
}
