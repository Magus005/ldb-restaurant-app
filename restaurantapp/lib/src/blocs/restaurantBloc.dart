import 'package:restaurantapp/src/models/restaurant_model.dart';
import 'package:rxdart/rxdart.dart';
import '../resources/repository.dart';

class RestaurantsBloc {
  final _repository = Repository();
  final _restaurantsFetcher = PublishSubject<RestaurantModel>();

  Stream<RestaurantModel> get allRestaurants => _restaurantsFetcher.stream;

  fetchAllRestaurants() async {
    RestaurantModel restaurantModel = await _repository.fetchAllRestaurants();
    _restaurantsFetcher.sink.add(restaurantModel);
  }

  dispose() {
    _restaurantsFetcher.close();
  }
}

final bloc = RestaurantsBloc();
